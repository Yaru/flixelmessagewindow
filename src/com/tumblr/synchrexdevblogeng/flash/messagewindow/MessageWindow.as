package com.tumblr.synchrexdevblogeng.flash.messagewindow 
{
	import org.flixel.*;
	/**
	 * ...
	 * @author Yaru
	 */
	public class MessageWindow 
	{
		private static var window:FlxSprite;
		private static var title:FlxText;
		private static var line:FlxSprite;
		private static var body:FlxText;
		private static var outlineLeft:FlxSprite;
		private static var outlineUp:FlxSprite;
		private static var outlineRight:FlxSprite;
		private static var outlineDown:FlxSprite;
		private static var textIcon:FlxSprite;
		public static var isShowing:Boolean;
		
		public static const RED:uint = 0xFFFF0000;
		public static const GREEN:uint = 0xFF00FF00;
		public static const BLUE:uint = 0xFF0000FF;
		public static const BLACK:uint = 0xFF000000;
		public static const WHITE:uint = 0xFFFFFFFF;
		public static const CYAN:uint = 0xFF00FFFF;
		public static const MAGENTA:uint = 0xFFFF00FF;
		public static const YELLOW:uint = 0xFFFFFF00;
		public static const LIGHT_GREY:uint = 0xFFCCCCCC;
		public static const MID_GREY:uint = 0xFF999999;
		public static const DARK_GREY:uint = 0xFF666666;
		
		/**
		 * Create a message window with the input parameters.
		 * @param	X
		 * @param	Y
		 * @param	Width 
		 * @param	Height 
		 * @param	Title 
		 * @param	TitleColor 
		 * @param	LineColor 
		 * @param	LineThickness 
		 * @param	Text 
		 * @param	TextColor 
		 * @param	BgColor 
		 * @param	OutlineThickness 
		 * @param	OutlineColor 
		 * @param	windowAlpha 
		 */
		
		public static function renderWindow(X:Number, Y:Number, Title:String, Text:String, Width:Number = 128, Height:Number = 128, TitleColor:uint = YELLOW, LineColor:uint = YELLOW, LineThickness:Number = 1, TextColor:uint = YELLOW, BgColor:uint = BLUE, OutlineThickness:Number = 1, OutlineColor:uint = RED, windowAlpha:Number = 0.5):void
		{
			window = new FlxSprite(X, Y);
			window.makeGraphic(Width, Height, BgColor);
			window.alpha = windowAlpha;
			FlxG.state.add(window);
			title = new FlxText(window.x,window.y, 100);
			title.text = Title;
			title.color = TitleColor;
			FlxG.state.add(title);
			line = new FlxSprite(title.x, title.y + 15);
			line.makeGraphic(window.frameWidth, LineThickness);
			line.color = LineColor;
			FlxG.state.add(line);
			body = new FlxText(window.x, window.y + 15, window.frameWidth);
			body.text = Text;
			body.color = TextColor;
			FlxG.state.add(body);
			
			outlineLeft = new FlxSprite(window.x - 1, window.y);
			outlineLeft.makeGraphic(OutlineThickness, window.frameHeight, OutlineColor);
			outlineUp = new FlxSprite(window.x, window.y - 1);
			outlineUp.makeGraphic(window.frameWidth, OutlineThickness, OutlineColor);
			outlineRight = new FlxSprite(window.x + window.frameWidth, window.y);
			outlineRight.makeGraphic(OutlineThickness, window.frameHeight + 1, OutlineColor);
			outlineDown = new FlxSprite(window.x, window.y + window.frameHeight);
			outlineDown.makeGraphic(window.frameWidth, OutlineThickness, OutlineColor);
			
			
			FlxG.state.add(outlineLeft);
			FlxG.state.add(outlineUp);
			FlxG.state.add(outlineRight);
			FlxG.state.add(outlineDown);
			
			isShowing = true;
		}
		
		/**
		 * Change the formatting of text in the body of the window.		 * @param	Font
		 * @param	Size
		 * @param	Color
		 * @param	Alignment 
		 * @param	ShadowColor
		 */
		
		public static function setBodyFormat(Font:String=null,Size:Number=8,Color:uint=0xffffff,Alignment:String=null,ShadowColor:uint=0):void
		{
			body.setFormat(Font, Size, Color, Alignment, ShadowColor);
		}
		
		/**
		 * Change the formatting of text in the title of the window.		 * @param	Font Nombre de la fuente que el texto usa.
		 * @param	Size 
		 * @param	Color 
		 * @param	Alignment
		 * @param	ShadowColor
		 */
		
		public static function setTitleFormat(Font:String=null,Size:Number=8,Color:uint=0xffffff,Alignment:String=null,ShadowColor:uint=0):void
		{
			title.setFormat(Font, Size, Color, Alignment, ShadowColor);
		}
		
		/**
		 * Change the position of the text.
		 * @param	X 
		 * @param	Y 
		 */
		
		public static function overrideBodyPos(X:Number = 0, Y:Number = 0):void
		{
			body.x = window.x + X;
			body.y = window.y + Y;
		}
		
		/**
		 * Returns a string with the Title.
		 * @return title.text
		 */
		
		public static function getTitle():String
		{
			return title.text;
		}
		
		/**
		 * Returns a string with the Body.
		 * @return body.text
		 */
		
		public static function getBody():String
		{
			return body.text;
		}
		
		/**
		 * Draws a icon
		 * @param	icon 
		 * @param	X 
		 * @param	Y 
		 * @param	iconWidth 
		 * @param	iconHeight 
		 * @param	animated 
		 */
		
		public static function drawIcon(icon:Class, X:Number, Y:Number,iconWidth:Number,iconHeight:Number,animated:Boolean):void
		{
			textIcon = new FlxSprite(body.x + X,body.y + Y);
			textIcon.loadGraphic(icon,animated,false,iconWidth,iconHeight);
			FlxG.state.add(textIcon);
		}
		
		/**
		 * Changes opacity.
		 * @param	a Alpha
		 */
		
		public static function setWindowAlpha(a:Number):void
		{
			window.alpha = a;
		}
		
		/**
		 * Returns opacity
		 * @return window.alpha
		 */
		
		public static function getWindowAlpha():Number
		{
			return window.alpha;
		}
		
		/**
		 * Destroys every sprite.
		 */
		
		public static function killWindow():void
		{
			FlxG.state.remove(window);
			FlxG.state.remove(title);
			FlxG.state.remove(line);
			FlxG.state.remove(body);
			FlxG.state.remove(outlineDown);
			FlxG.state.remove(outlineLeft);
			FlxG.state.remove(outlineRight);
			FlxG.state.remove(outlineUp);
			FlxG.state.remove(textIcon);
			isShowing = false;
		}
		
		
	}

}