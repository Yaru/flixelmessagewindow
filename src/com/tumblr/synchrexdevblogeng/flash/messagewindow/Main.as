package com.tumblr.synchrexdevblogeng.flash.messagewindow
{
	import org.flixel.*;
	import com.tumblr.synchrexdevblogeng.flash.messagewindow.PlayState
	import flash.system.fscommand;
	
	[SWF(width = "640", height = "480",backgroundColor = 0x000000)]
	
	/**
	 * ...
	 * @author Yaru
	 */
	public class Main extends FlxGame
	{
		
		public function Main():void 
		{
			super(320, 240, PlayState, 2);
		}
		
		override protected function update():void 
		{
			if (FlxG.keys.justPressed("ESCAPE")) {
				fscommand("quit");
			}
			super.update();
		}
		
		
	}
	
}