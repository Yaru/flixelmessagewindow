package com.tumblr.synchrexdevblogeng.flash.messagewindow 
{
	import org.flixel.FlxG;
	import org.flixel.FlxState;
	import com.tumblr.synchrexdevblogeng.flash.messagewindow.MessageWindow;
	/**
	 * ...
	 * @author Yaru
	 */
	public class PlayState extends FlxState
	{
		private var n:int;
		
		[Embed(source = "../../../../../../assets/mustache.png")] private static const MUSTACHE:Class;
		
		override public function update():void 
		{
			if (MessageWindow.isShowing == false) {
				switch (n) 
				{
					case 0:
						MessageWindow.renderWindow(95, 50, "        HI HI!", "PRESS SPACE!");
						MessageWindow.setBodyFormat(null, 30, MessageWindow.YELLOW, "center", MessageWindow.BLACK);
						MessageWindow.setTitleFormat(null, 8, MessageWindow.YELLOW, "center", MessageWindow.BLACK);
						MessageWindow.overrideBodyPos(0, 30);
					break;
					
					case 1:
						MessageWindow.renderWindow(95, 50, "LOOK AT ME!", "I'm a fully configurable sign!",128,64);
					break;
					
					case 2:
						MessageWindow.renderWindow(95, 50, "I CAN CHANGE...", "MY SIZE AND MY COLORS!", 128, 150, MessageWindow.RED, MessageWindow.BLACK, 1, MessageWindow.BLUE, MessageWindow.DARK_GREY, 1, MessageWindow.YELLOW, 0.5);
					break;
					
					case 3:
						MessageWindow.renderWindow(95, 50, "AND...", "I can draw sprites!");
						MessageWindow.drawIcon(MUSTACHE, 0, 20, 16, 16, false);
					break;
					
					case 4:
						MessageWindow.renderWindow(95, 50, "CHECK MY CODE...", "And have fun!", 128, 64);
					break;
				}
			}
			
			if (FlxG.keys.justPressed("SPACE")) {
				MessageWindow.killWindow();
				if (n < 4) {
					n++
				}
				else {
					n = 0;
				}
			}
		}
		
		
	}

}